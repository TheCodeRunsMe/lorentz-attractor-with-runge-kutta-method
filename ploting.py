import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np


fig = plt.figure("Lorenz Attractor", figsize=(10, 10), facecolor="#af763a")
fig.tight_layout()
ax = fig.add_subplot(projection="3d")


def update_lines(num, points):
    ax.clear()
    ax.plot(*points[:num, :].T, c='#186013', linewidth=.5)
    ax.set(xlim3d=[np.max(points[:, 0]), np.min(points[:, 0])], xlabel='X',
           ylim3d=[np.max(points[:, 1]), np.min(points[:, 1])], ylabel='Y',
           zlim3d=[np.max(points[:, 2]), np.min(points[:, 2])], zlabel='Z',
           facecolor="#bfbfbf")
    ax.view_init(6, -80)
    return ax


def plot(points):
    ani = animation.FuncAnimation(fig, update_lines, points.shape[0], fargs=(points,),
                                  interval=1)
    ani.save("lorenz attractor.mp4")
    plt.show()
    figure, ax = plt.subplots(3,1)
    figure.suptitle("Dimension according to time")
    for i in range(3):
        ax[i].plot(points[:,i])
        ax[i].set_xlabel("Time")
        ax[i].set_ylabel(f"Dimension {i}")
        ax[i].grid()
    plt.show()
    figure.savefig("Dims.png", dpi=300, format="png")
    