# Computational and Numerical Techniques 2021-2022 Autumn Semester Final Project
## Project members:
- Hemantika Sengar
- Özgür Özer

## Environment
- Python 3.10.1 with venv
- Gnu/Linux 64 bit
### Site packages:
1. Numpy
2. Matplotlib

### Commanline inputs:
```
Please enter time steps: .01
Please enter number of steps: 10000
Please enter initial x: 10
Please enter initial y: 10
Please enter initial z: 10
Enter sigma, leave blank for stable values: 
```

![Demo with default values](lorenz attractor.mp4)

![Demo with default values](Dims.png)