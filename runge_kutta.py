import numpy as np


def RG(r, n, dt, f):
    """
    Calculates the next n points with dt steps using t independent ode
    system f where f retuns the differential equations as an array
    """
    for i in range(n):
        k1 = dt * f(r[-1])
        k2 = dt * f(r[-1] + k1 / 2)
        k3 = dt * f(r[-1] + k2 / 2)
        k4 = dt * f(r[-1] + k3)
        r = np.vstack((r, r[-1] + (k1 + 2 * k2 + 2 * k3 + k4) / 6))
    return r
