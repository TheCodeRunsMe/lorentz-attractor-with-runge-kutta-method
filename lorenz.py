import numpy as np
from runge_kutta import RG
from ploting import plot

dt = float(input("Please enter time steps: "))
if dt < 0: raise ValueError("dt cannot be lower than 0")
N = int(input("Please enter number of steps: "))
if N < 1: raise ValueError("N cannot be lower than 1")
x0 = np.array([float(input(f"Please enter initial {i}: "))
              for i in ["x", "y", "z"]], dtype=float)
x0 = x0[:, np.newaxis].T  # make it a matrix with rows as different iterations


def specifyParams(f, sigma, rho, beta):
    def specified(x):
        return f(x, sigma, rho, beta)
    return specified


def lorenz(x, s=10., r=28., b=8./3.):
    "returns differential equation system as an array"
    return np.array(
    [s * (x[1] - x[0]), r * x[0] - x[1] - x[0] * x[2], x[0] * x[1] - b * x[2]])


sigma = input("Enter sigma, leave blank for stable values: ")
if sigma: 
    rho = float(input("Enter rho: "))
    beta = float(input("Enter beta"))
    points = RG(x0, N, dt, specifyParams(lorenz, float(sigma), rho, beta))
else: points = RG(x0, N, dt, lorenz)


plot(points)
